package hu.wga.wgabrowser;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.widget.SearchView;
import android.view.View;

import com.opencsv.CSVReader;

import org.apache.commons.collections.ArrayStack;
import org.apache.commons.lang3.SystemUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Tomasz on 2017-05-29.
 */

@Getter
@Setter
public final class Catalog {
    static final int ENTRIES_COUNT = 43456;
    static final String SHARED_PREFERENCES_NAME = "SP_WGA_HU";
    private BrowserActivity browserActivity;
    private ArtworkActivity artworkActivity;
    private List<CatalogEntry> entries = new ArrayList<>();
    private List<CatalogEntry> discoverEntries = new ArrayList<>();
    private List<CatalogEntry> favoriteEntries = new ArrayList<>();
    private List<String> authors = new ArrayList<>();
    private HashMap<String, List<CatalogEntry>> entriesByAuthor = new HashMap<>();

    private SharedPreferences getSharedPreferences() {
        if (browserActivity != null) {
            return browserActivity.getSharedPreferences(SHARED_PREFERENCES_NAME, 0);
        } else {
            return artworkActivity.getSharedPreferences(SHARED_PREFERENCES_NAME, 0);
        }
    }

    public void saveFavorites() {
        SharedPreferences sp = getSharedPreferences();

        for (CatalogEntry entry : entries) {
            if (entry.isFavorite()) {
                sp.edit().putBoolean("favorite" + entry.getId(), entry.isFavorite()).commit();
            }
        }
    }

    public List<CatalogEntry> searchByTitle(String title) {
        String[] phrases = title.split(" ");
        List<CatalogEntry> ret = new ArrayList<>();
        for (CatalogEntry entry : entries) {
            boolean satisfiesAll = true;
            for (String phrase : phrases) {
                if (entry.getTitle().toLowerCase().contains(phrase.toLowerCase()) || entry.getAuthor().toLowerCase().contains(phrase.toLowerCase())) {

                } else {
                    satisfiesAll = false;
                }
            }
            if (satisfiesAll) {
                ret.add(entry);
            }
        }
        return ret;
    }

    public void toggleFavorite(CatalogEntry entry) {
        if (entry.isFavorite()) {
            entry.setFavorite(false);
        } else {
            entry.setFavorite(true);
        }
        SharedPreferences sp = getSharedPreferences();
        int id = entry.getId();
        sp.edit().putBoolean("favorite" + id, entry.isFavorite()).commit();
    }

    public boolean getFavorite(CatalogEntry entry) {
        SharedPreferences sp = getSharedPreferences();
        int id = entry.getId();
        if (sp.getBoolean("favorite" + id, false)) {
            entry.setFavorite(true);
        } else {
            entry.setFavorite(false);
        }
        return entry.isFavorite();
    }

    public void generateDiscoverEntries() {
        Random r = new Random();
        List<Integer> used = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            int idx = r.nextInt(ENTRIES_COUNT);
            while (used.contains(idx))
                idx = r.nextInt(ENTRIES_COUNT);
            used.add(idx);
            discoverEntries.add(entries.get(idx));
        }
    }

    public static Catalog getInstance(BrowserActivity context) {
        Catalog catalog = new Catalog();
        catalog.browserActivity = context;
        return catalog;
    }

    public static Catalog getInstance(ArtworkActivity context) {
        Catalog catalog = new Catalog();
        catalog.artworkActivity = context;
        return catalog;
    }

    public void loadFavorites() {
        favoriteEntries.clear();
        SharedPreferences sp = getSharedPreferences();
        for (String key : sp.getAll().keySet()) {
            if (key.contains("favorite") && sp.getBoolean(key, false)) {
                int id = Integer.valueOf(key.replace("favorite", ""));
                CatalogEntry entry = entries.get(id);
                entry.setFavorite(true);
                favoriteEntries.add(entry);
            }
        }
    }

    public List<CatalogEntry> getFavorites() {
        loadFavorites();
        return favoriteEntries;
    }

    public void readAssetsAsync() {
        AsyncTask<Void, Integer, Void> asyncTask = new AsyncTask<Void, Integer, Void>() {

            @Override
            protected void onProgressUpdate(Integer... values) {
                super.onProgressUpdate(values);
                browserActivity.updateProgress(values[0]);
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                generateDiscoverEntries();
                browserActivity.loadDiscoverFragment();
                SearchView titleSearchView = (SearchView)browserActivity.findViewById(R.id.titleSearchView);
                titleSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        browserActivity.loadSearchFragment(query);
                        DrawerLayout drawer = (DrawerLayout) browserActivity.findViewById(R.id.drawer_layout);
                        drawer.closeDrawer(GravityCompat.START);
                        return true;
                    }

                    @Override
                    public boolean onQueryTextChange(String newText) {
                        return false;
                    }
                });

            }

            @Override
            protected Void doInBackground(Void... params) {
                //assert (browserActivity != null);
                try {
                    InputStream is = browserActivity.getAssets().open("catalog.csv");
                    InputStreamReader isr = new InputStreamReader(is);
                    CSVReader reader = new CSVReader(isr, ';');
                    int progress = 0;
                    String [] nextLine;
                    while ((nextLine = reader.readNext()) != null) {
                        if (progress != 0) {
                            CatalogEntry entry = new CatalogEntry(nextLine, progress-1);
                            entries.add(entry);
                            if (authors.isEmpty() || (!authors.isEmpty() && !authors.get(authors.size()-1).equals(entry.getAuthor()))) {
                                authors.add(entry.getAuthor());
                            }
                            if (entriesByAuthor.get(entry.getAuthor()) == null) {
                                entriesByAuthor.put(entry.getAuthor(), new ArrayList<CatalogEntry>());
                            }
                            entriesByAuthor.get(entry.getAuthor()).add(entry);
                        }
                        this.publishProgress(progress);
                        progress++;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }
        };
        asyncTask.execute();
    }


}
