package hu.wga.wgabrowser;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import jp.wasabeef.picasso.transformations.BlurTransformation;

import static hu.wga.wgabrowser.R.id.container;

public class ArtworkActivity extends AppCompatActivity {

    private Catalog catalog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        catalog = Catalog.getInstance(this);
        setContentView(R.layout.activity_artwork);
        final CatalogEntry catalogEntry = getIntent().getExtras().getParcelable("catalogEntry");
        ImageView imageView = (ImageView)findViewById(R.id.artworkImageView);
        Picasso.with(getApplicationContext()).load(catalogEntry.getImageURL()).into(imageView);
        TextView titleTextView = (TextView)findViewById(R.id.artworkTitleTextView);
        TextView authorTextView = (TextView)findViewById(R.id.artworkAuthorTextView);
        authorTextView.setPaintFlags(authorTextView.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);
        TextView dateTextView = (TextView)findViewById(R.id.artworkDateTextView);
        titleTextView.setText(catalogEntry.getTitle());
        authorTextView.setText(catalogEntry.getAuthor());
        dateTextView.setText(catalogEntry.getDate());
        ImageButton browserButton = (ImageButton)findViewById(R.id.button_browser);
        browserButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse(catalogEntry.getUrl()));
                startActivity(intent);
            }
        });
        final Intent resultIntent = new Intent();
        final ImageButton favoriteButton = (ImageButton)findViewById(R.id.button_favorite);
        if (catalogEntry.isFavorite()) {
            favoriteButton.setImageResource(R.drawable.fa_heart_shape_silhouette);
        } else {
            favoriteButton.setImageResource(R.drawable.fa_heart_shape_outline);
        }
        favoriteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                catalog.toggleFavorite(catalogEntry);
                if (catalogEntry.isFavorite()) {
                    favoriteButton.setImageResource(R.drawable.fa_heart_shape_silhouette);
                } else {
                    favoriteButton.setImageResource(R.drawable.fa_heart_shape_outline);
                }
            }
        });
        authorTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra("author", catalogEntry.getAuthor());
                setResult(RESULT_OK, intent);
                finish();
            }
        });
        ImageButton shareButton = (ImageButton) findViewById(R.id.button_share);
        shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareEntry(catalogEntry);
            }
        });
    }

    public void shareEntry(CatalogEntry entry) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_TEXT, entry.getUrl());
        intent.setType("text/plain");
        startActivity(Intent.createChooser(intent, "Share artwork to"));
    }

}
