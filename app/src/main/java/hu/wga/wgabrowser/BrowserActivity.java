package hu.wga.wgabrowser;

import android.app.Fragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import lombok.Getter;
import lombok.Setter;
import xyz.danoz.recyclerviewfastscroller.vertical.VerticalRecyclerViewFastScroller;

@Getter
@Setter
public final class BrowserActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, DiscoverFragment.OnListFragmentInteractionListener, ByAuthorFragment.OnListFragmentInteractionListener,
        AuthorsFragment.OnListFragmentInteractionListener, FavoritesFragment.OnListFragmentInteractionListener, SearchFragment.OnListFragmentInteractionListener {

    private Catalog catalog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browser);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        loadLoadingFragment();
        catalog = Catalog.getInstance(this);
        catalog.readAssetsAsync();

    }

    public void loadSearchFragment(String query) {
        setTitle("Search WGA: " + query);
        FragmentManager fm = getFragmentManager();
        SearchFragment fragment = new SearchFragment();
        fragment.setSearchQuery(query);
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.container, fragment);
        ft.commit();
    }

    public void loadFavoritesFragment() {
        setTitle("Favorites");
        FragmentManager fm = getFragmentManager();
        FavoritesFragment fragment = new FavoritesFragment();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.container, fragment);
        ft.commit();
    }

    public void loadAuthorsFragment() {
        setTitle("Browse by artist");
        FragmentManager fm = getFragmentManager();
        AuthorsFragment fragment = new AuthorsFragment();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.container, fragment);
        ft.commit();
    }

    public void loadByAuthorFragment(String author) {
        setTitle("Artist: " + author);
        FragmentManager fm = getFragmentManager();
        ByAuthorFragment fragment = new ByAuthorFragment();
        fragment.setAuthor(author);
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.container, fragment);
        ft.commit();
    }

    public void loadLoadingFragment() {
        setTitle("Loading...");
        FragmentManager fm = getFragmentManager();
        LoadingFragment fragment = new LoadingFragment();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.container, fragment);
        ft.commit();
    }

    public void updateProgress(int progress) {
        ProgressBar progressBar = (ProgressBar)findViewById(R.id.loadingProgressBar);
        progressBar.setProgress(progress);
        TextView textView = (TextView)findViewById(R.id.loadingTextView);
        textView.setText("Loading database: " + progress + "/" + Catalog.ENTRIES_COUNT);
    }

    public void shareEntry(CatalogEntry entry) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_TEXT, entry.getUrl());
        intent.setType("text/plain");
        startActivity(Intent.createChooser(intent, "Share artwork to"));
    }

    public void loadDiscoverFragment() {
        setTitle("Discover");
        FragmentManager fm = getFragmentManager();
        DiscoverFragment fragment = DiscoverFragment.newInstance(1);
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.container, fragment);
        ft.commit();
    }

    public void loadArtworkActivity(CatalogEntry catalogEntry) {
        Intent artworkIntent = new Intent(this, ArtworkActivity.class);
        artworkIntent.putExtra("catalogEntry", catalogEntry);
        startActivityForResult(artworkIntent, 1);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if(resultCode == RESULT_OK) {
                loadByAuthorFragment(data.getStringExtra("author"));
            }
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.browser, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_discover) {
            loadDiscoverFragment();
        } else if (id == R.id.nav_artists) {
            loadAuthorsFragment();
        } else if (id == R.id.nav_favorites) {
            loadFavoritesFragment();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onListFragmentInteraction(CatalogEntry item) {
        //loadArtworkFragment(item);
        loadArtworkActivity(item);
    }

    @Override
    public void onListFragmentInteraction(String item) {
        loadByAuthorFragment(item);
    }
}
