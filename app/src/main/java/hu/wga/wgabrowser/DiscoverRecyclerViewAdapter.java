package hu.wga.wgabrowser;

import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import hu.wga.wgabrowser.DiscoverFragment.OnListFragmentInteractionListener;

import java.util.List;

public class DiscoverRecyclerViewAdapter extends RecyclerView.Adapter<DiscoverRecyclerViewAdapter.ViewHolder> {

    private final List<CatalogEntry> mValues;
    private final OnListFragmentInteractionListener mListener;

    public DiscoverRecyclerViewAdapter(List<CatalogEntry> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_entry, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.setItem(mValues.get(position));

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mIdView;
        public final TextView mContentView;
        public final ImageView imageView;
        public final TextView upperLeftView;
        public final ImageButton favoriteButton;
        public CatalogEntry mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mIdView = (TextView) view.findViewById(R.id.id);
            mContentView = (TextView) view.findViewById(R.id.content);
            imageView = (ImageView) view.findViewById(R.id.image_view_miniature);
            upperLeftView = (TextView) view.findViewById(R.id.upper_left);
            favoriteButton = (ImageButton) view.findViewById(R.id.button_favorite);
        }

        public ViewHolder setItem(CatalogEntry item) {
            this.mItem = item;
            Picasso.with(mView.getContext()).load(mItem.getImageURL()).into(imageView);
            favoriteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((BrowserActivity)v.getContext()).getCatalog().toggleFavorite(mItem);
                    if (mItem.isFavorite()) {
                        favoriteButton.setImageResource(R.drawable.fa_heart_shape_silhouette);
                    } else {
                        favoriteButton.setImageResource(R.drawable.fa_heart_shape_outline);
                    }
                }
            });
            if (mItem.isFavorite()) {
                favoriteButton.setImageResource(R.drawable.fa_heart_shape_silhouette);
            } else {
                favoriteButton.setImageResource(R.drawable.fa_heart_shape_outline);
            }
            mIdView.setText(item.getTitle());
            mContentView.setText(item.getAuthor());
            upperLeftView.setText(item.getDate());

            return this;
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}
