package hu.wga.wgabrowser;

import android.os.Parcel;
import android.os.Parcelable;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Tomasz on 2017-05-29.
 */

@Getter
@Setter
public class CatalogEntry implements Parcelable {
    private String title;
    private String author;
    private int id;
    private String url;
    private String date;
    private boolean favorite = false;

    private CatalogEntry() {}

    public CatalogEntry(String [] line, int id) {
        this.id = id;
        this.title = line[2];
        this.date = line[3];
        this.author = line[0];
        this.url = line[6];
    }

    public String getImageURL() {
        String ret = url.replaceFirst("html", "detail");
        ret = ret.replaceFirst("html", "jpg");
        return ret;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(author);
        dest.writeInt(id);
        dest.writeString(url);
        dest.writeString(date);
        dest.writeString(Boolean.toString(favorite));
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        @Override
        public CatalogEntry createFromParcel(Parcel in) {
            CatalogEntry catalogEntry = new CatalogEntry();
            catalogEntry.setTitle(in.readString());
            catalogEntry.setAuthor(in.readString());
            catalogEntry.setId(in.readInt());
            catalogEntry.setUrl(in.readString());
            catalogEntry.setDate(in.readString());
            catalogEntry.setFavorite(Boolean.valueOf(in.readString()));
            return catalogEntry;
        }

        @Override
        public Object[] newArray(int size)
        {
            return new CatalogEntry[size];
        }
    };
}
